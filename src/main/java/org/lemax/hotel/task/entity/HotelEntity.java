package org.lemax.hotel.task.entity;

import lombok.Data;
import org.lemax.hotel.task.model.GeoLocation;
import org.lemax.hotel.task.model.Hotel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class HotelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String hotelName;
    private String hotelPrice;
    private String latitude;
    private String longitude;

    public HotelEntity(){}

    public HotelEntity(Hotel hotel) {
        this.id = hotel.getId();
        this.hotelName = hotel.getHotelName();
        this.hotelPrice = hotel.getHotelPrice();
        this.latitude = hotel.getHotelGeoLocation().getLatitude();
        this.longitude = hotel.getHotelGeoLocation().getLongitude();
    }

    public HotelEntity(String hotelName, String hotelPrice,
                       String latitude, String longitude){
        this.hotelName = hotelName;
        this.hotelPrice = hotelPrice;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public HotelEntity(String hotelName, String hotelPrice,
                       GeoLocation geoLocation){
        this.hotelName = hotelName;
        this.hotelPrice = hotelPrice;
        this.latitude = geoLocation.getLatitude();
        this.longitude = geoLocation.getLongitude();
    }
}
