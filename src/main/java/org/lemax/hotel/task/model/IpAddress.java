package org.lemax.hotel.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class IpAddress {

    @JsonProperty("ip")
    private String ip;

    @Override
    public String toString(){
        return ip;
    }
}
