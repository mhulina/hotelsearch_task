package org.lemax.hotel.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.lemax.hotel.task.entity.HotelEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hotel {

    private long id;
    @JsonProperty("name")
    private String hotelName;
    @JsonProperty("price")
    private String hotelPrice;
    @JsonProperty("geoLocation")
    private GeoLocation hotelGeoLocation;
    @JsonProperty("distance")
    private double distance;

    public Hotel(){
    }
    public Hotel(final Long id,
                 final String hotelName,
              final String hotelPrice,
              final GeoLocation hotelGeoLocation){
        this.id = id;
        this.hotelName = hotelName;
        this.hotelPrice = hotelPrice;
        this.hotelGeoLocation = hotelGeoLocation;
    }

    public Hotel(final HotelEntity hotelEntity){
        this.id = hotelEntity.getId();
        this.hotelName = hotelEntity.getHotelName();
        this.hotelPrice = hotelEntity.getHotelPrice();
        this.hotelGeoLocation = new GeoLocation(
            hotelEntity.getLatitude(),
            hotelEntity.getLongitude()
        );
    }
}
