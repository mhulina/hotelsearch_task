package org.lemax.hotel.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoLocation {

    @JsonProperty("latitude")
    private String latitude;
    @JsonProperty("longitude")
    private String longitude;

    public GeoLocation(){

    }

    public GeoLocation(String latitude, String longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
