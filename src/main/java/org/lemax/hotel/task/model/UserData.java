package org.lemax.hotel.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserData {

    @JsonProperty("query")
    private String query;
    @JsonProperty("status")
    private String status;
    @JsonProperty("country")
    private String country;
    @JsonProperty("countryCode")
    private String countryCode;
    @JsonProperty("regionName")
    private String regionName;
    @JsonProperty("city")
    private String city;
    @JsonProperty("lat")
    private double lat;
    @JsonProperty("lon")
    private double lon;

}
