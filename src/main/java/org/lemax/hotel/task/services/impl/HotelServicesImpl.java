package org.lemax.hotel.task.services.impl;

import org.lemax.hotel.task.dao.HotelDao;
import org.lemax.hotel.task.dao.UserDao;
import org.lemax.hotel.task.entity.HotelEntity;
import org.lemax.hotel.task.model.GeoLocation;
import org.lemax.hotel.task.model.Hotel;
import org.lemax.hotel.task.model.UserData;
import org.lemax.hotel.task.services.HotelServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotelServicesImpl implements HotelServices {

    private final HotelDao hotelDao;
    private final UserDao userDao;

    private static final Logger LOG = LoggerFactory.getLogger(HotelServicesImpl.class);

    private static final double SEARCH_RADIUS = 10;
    private static final double AVERAGE_LITER_PER_100 = 6.5;
    private static final double DIESEL_PRICE = 12.8;

    public HotelServicesImpl(final HotelDao hotelDao,
                             final UserDao userDao){
        this.hotelDao = hotelDao;
        this.userDao = userDao;
    }

    @Override
    public List<Hotel> getHotels() {
        UserData userGeoLocation = userDao.getUserGeoLocation(userDao.getUserIpAddress());
        List<HotelEntity> hotelsEntity = hotelDao.getListOfHotels();
        List<Hotel> hotels = new ArrayList<>();

        for(HotelEntity hotelEntity : hotelsEntity){
            Hotel hotel = new Hotel(
                hotelEntity.getId(),
                hotelEntity.getHotelName(),
                hotelEntity.getHotelPrice(),
                new GeoLocation(
                    hotelEntity.getLatitude(),
                    hotelEntity.getLongitude()
                )
            );
            hotel.setDistance(
                    calculateDistance(
                            hotel.getHotelGeoLocation(),
                            userGeoLocation
                    )
            );

            hotels.add(hotel);
        }

        return hotels;
    }

    @Override
    public Hotel createHotel(Hotel hotel) {
        hotelDao.createHotel(
            new HotelEntity(hotel)
        );

        return hotel;
    }

    @Override
    public List<Hotel> createHotels(List<Hotel> hotels) {
        for(Hotel hotel : hotels){
            LOG.info("Creating hotel: " + hotel);
            hotelDao.createHotel(new HotelEntity(hotel));
        }

        return hotels;
    }

    @Override
    public void deleteHotelById(Long id) {
        LOG.info("Removing hotel: " + hotelDao.getHotelById(id));
        hotelDao.deleteHotel(id);
    }

    @Override
    public Hotel getHotelById(Long id){
        HotelEntity hotelEntity = hotelDao.getHotelById(id);
        Hotel hotel = new Hotel(hotelEntity);
        hotel.setDistance(
                calculateDistance(
                        hotel.getHotelGeoLocation(),
                        userDao.getUserGeoLocation(
                                userDao.getUserIpAddress()
                        )
                )
        );

        return hotel;
    }

    @Override
    public Hotel updateHotelData(Hotel newHotelData, Long id){
        HotelEntity oldHotel = hotelDao.getHotelById(id);

        LOG.info("Updating " + oldHotel + "\nwith " + newHotelData);

        if(null != oldHotel){
            oldHotel.setHotelName(
                null != newHotelData.getHotelName() ?
                    newHotelData.getHotelName():
                    null
            );
            oldHotel.setHotelPrice(
                null != newHotelData.getHotelPrice() ?
                    newHotelData.getHotelPrice():
                    null
            );
            oldHotel.setLatitude(
                null != newHotelData.getHotelGeoLocation().getLatitude() ?
                    newHotelData.getHotelGeoLocation().getLatitude() :
                    null
            );
            oldHotel.setLongitude(
                null != newHotelData.getHotelGeoLocation().getLongitude() ?
                    newHotelData.getHotelGeoLocation().getLongitude() :
                    null
            );

            hotelDao.createHotel(oldHotel);

            return new Hotel(oldHotel);
        }
        else{
            newHotelData.setId(id);

            hotelDao.createHotel(new HotelEntity(newHotelData));

            return newHotelData;
        }
    }

    @Override
    public List<Hotel> searchHotels() {
        UserData userGeoLocation = userDao.getUserGeoLocation(userDao.getUserIpAddress());
        List<Hotel> hotels = getHotels();
        List<Hotel> hotelsInRadius = new ArrayList<>();
        List<Hotel> hotelsOutsideRadius = new ArrayList<>();
        List<Hotel> sortedHotels;

        for(Hotel hotel : hotels){
            hotel.setDistance(calculateDistance(hotel.getHotelGeoLocation(), userGeoLocation));

            if(SEARCH_RADIUS >= hotel.getDistance()){
                hotelsInRadius.add(hotel);
                continue;
            }

            hotelsOutsideRadius.add(hotel);
        }

        Comparator<Hotel> comparator = Comparator.comparing(hotel -> hotel.getDistance());
        comparator = comparator.thenComparing(Comparator.comparing(hotel -> Double.parseDouble(hotel.getHotelPrice())));
        Comparator<Hotel> outsideRadius = Comparator.comparing(hotel ->
                ((hotel.getDistance() * AVERAGE_LITER_PER_100 * DIESEL_PRICE) / 100) + Double.parseDouble(hotel.getHotelPrice())
                );

        sortedHotels = hotelsInRadius.stream().sorted(comparator).collect(Collectors.toList());
        sortedHotels.addAll(hotelsOutsideRadius.stream().sorted(outsideRadius).collect(Collectors.toList()));

        return sortedHotels;
    }

    private double calculateDistance(GeoLocation geoLocation, UserData userData){
        double theta = userData.getLon() - Double.parseDouble(geoLocation.getLongitude());
        double dist = Math.sin(deg2rad(userData.getLat())) *
                        Math.sin(deg2rad(Double.parseDouble(geoLocation.getLatitude()))) +
                        Math.cos(deg2rad(userData.getLat())) *
                        Math.cos(deg2rad(Double.parseDouble(geoLocation.getLatitude()))) *
                                Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;

        BigDecimal rounding = BigDecimal.valueOf(dist);
        rounding = rounding.setScale(2, RoundingMode.HALF_UP);

        return rounding.doubleValue();
    }

    private double deg2rad(double deg){
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad){
        return (rad * 180.0 / Math.PI);
    }
}
