package org.lemax.hotel.task.services.impl;

import org.lemax.hotel.task.dao.UserDao;
import org.lemax.hotel.task.services.UserServices;
import org.springframework.stereotype.Service;

@Service
public class UserServicesImpl implements UserServices {

    private final UserDao userDao;

    public UserServicesImpl(final UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String retrieveUserIp(){
        return userDao.getUserIpAddress();
    }
}
