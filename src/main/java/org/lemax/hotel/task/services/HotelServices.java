package org.lemax.hotel.task.services;

import org.lemax.hotel.task.model.Hotel;

import java.util.List;

public interface HotelServices {
    List<Hotel> getHotels();
    Hotel createHotel(Hotel hotel);
    List<Hotel> createHotels(List<Hotel> hotels);
    void deleteHotelById(Long id);
    Hotel getHotelById(Long id);
    Hotel updateHotelData(Hotel newHotelData, Long id);
    List<Hotel> searchHotels();
}
