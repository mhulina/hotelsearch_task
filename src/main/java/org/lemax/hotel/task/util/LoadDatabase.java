package org.lemax.hotel.task.util;

import org.lemax.hotel.task.entity.HotelEntity;
import org.lemax.hotel.task.model.GeoLocation;
import org.lemax.hotel.task.model.Hotel;
import org.lemax.hotel.task.repository.HotelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

    private static final Logger LOG = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(HotelRepository repository){
        return args -> {
            LOG.info("Preloading " + repository.save(new HotelEntity("Esplanade", "950", "45.80529590946047", "15.975991000000002")));
            LOG.info("Preloading " + repository.save(new HotelEntity("Sheraton", "600", new GeoLocation("45.80746670084368", "15.984868676703766"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Rixos Premium Dubrovnik", "3000", new GeoLocation("42.64595538391997", "18.09099341534076"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Hotel Bellevue", "5000", new GeoLocation("42.64742353467189", "18.092688692044515"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Hotel Kompas", "2000", new GeoLocation("42.65716745637457", "18.069796553977742"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Radisson Blu Resort & Spa", "2600", new GeoLocation("43.503312885650494", "16.470082684659246"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Cornaro Hotel", "1800", new GeoLocation("43.5107396711057", "16.439390792044517"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Grand Park Hotel", "5300", new GeoLocation("45.07626447925321", "13.6370313"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("TUI BLUE Adriatic Beach", "2850", new GeoLocation("43.18585165588864", "17.163038030681502"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Amfora Hvar Grand Beach Resort", "2400", new GeoLocation("43.17347130503707", "16.43372294602226"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Park Plaza Histria", "1900", new GeoLocation("44.836571835674626", "13.839606430681508"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Canopy by Hilton", "600", new GeoLocation("45.806427421699766", "15.985252430681504"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Valamar Meteor Hotel", "1600", new GeoLocation("43.299585643889905", "17.014759269318496"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Park Plaza Arena", "2000", new GeoLocation("44.83711028912042", "13.829661915340749"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Hotel Luxe", "1700", new GeoLocation("43.50705558204655", "16.443903492044516"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Falkensteiner Club Funimation", "2500", new GeoLocation("44.13731224798821", "15.211176225086316"))));
            LOG.info("Preloading " + repository.save(new HotelEntity("Amphora", "2000", new GeoLocation("43.502784804011995", "16.482020484659248"))));
        };
    }
}
