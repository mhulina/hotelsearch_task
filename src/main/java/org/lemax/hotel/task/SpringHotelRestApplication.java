package org.lemax.hotel.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHotelRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringHotelRestApplication.class, args);
    }
}
