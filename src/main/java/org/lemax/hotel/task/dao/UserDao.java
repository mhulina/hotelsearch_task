package org.lemax.hotel.task.dao;

import org.lemax.hotel.task.model.UserData;

public interface UserDao {

    String getUserIpAddress();

    UserData getUserGeoLocation(String ipAddress);
}
