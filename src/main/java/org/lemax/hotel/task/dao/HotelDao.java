package org.lemax.hotel.task.dao;

import org.lemax.hotel.task.entity.HotelEntity;


import java.util.List;

public interface HotelDao {
    List<HotelEntity> getListOfHotels();
    HotelEntity createHotel(HotelEntity hotel);
    void deleteHotel(Long id);
    HotelEntity getHotelById(Long id);
}
