package org.lemax.hotel.task.dao.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.lemax.hotel.task.dao.UserDao;
import org.lemax.hotel.task.model.IpAddress;
import org.lemax.hotel.task.model.UserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URL;

@Repository
public class UserDaoImpl implements UserDao {

    private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class);
    private final String ipAddressEndpoint;
    private final String geoLocationEndpoint;

    @Autowired
    public UserDaoImpl(final @Value("${app.ipaddress.url}") String ipAddressEndpoint,
                       final @Value("${app.geolocation.url}") String geoLocationEndpoint){
        this.ipAddressEndpoint = ipAddressEndpoint;
        this.geoLocationEndpoint = geoLocationEndpoint;
    }

    @Override
    public String getUserIpAddress() {
        IpAddress ip = null;
        try{
            URL url = new URL(ipAddressEndpoint);

            ObjectMapper mapper = new ObjectMapper();
            ip = mapper.readValue(url, IpAddress.class);
        }catch (IOException e){
            LOG.info("Error in getting user IP Address.");
            e.printStackTrace();
        }

        return ip.toString();
    }

    @Override
    public UserData getUserGeoLocation(String ipAddress) {
        UserData userGeoLocation = null;
        try{
            URL url = new URL(geoLocationEndpoint + getUserIpAddress());

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            userGeoLocation = mapper.readValue(url, UserData.class);
        }catch (IOException e){
            LOG.info("Error in getting user geo location.");
            e.printStackTrace();
        }

        return userGeoLocation;
    }
}
