package org.lemax.hotel.task.dao.impl;

import org.lemax.hotel.task.dao.HotelDao;
import org.lemax.hotel.task.entity.HotelEntity;
import org.lemax.hotel.task.repository.HotelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HotelDaoImpl implements HotelDao {

    private final HotelRepository hotelRepository;

    private static final Logger LOG = LoggerFactory.getLogger(HotelDaoImpl.class);

    @Autowired
    public HotelDaoImpl(final HotelRepository hotelRepository){
        this.hotelRepository = hotelRepository;
    }


    @Override
    public List<HotelEntity> getListOfHotels() {
        return hotelRepository.findAll();
    }

    @Override
    public HotelEntity createHotel(HotelEntity hotel) {
        try{
            hotelRepository.save(hotel);
            LOG.info("Creation successful for " + hotel);

        }catch (Exception e){
            LOG.info("Exception in createHotel method for: " + hotel);
        }

        return hotel;
    }


    @Override
    public void deleteHotel(Long id) {
        try{
            hotelRepository.deleteById(id);

            LOG.info("Hotel at ID: " + id + " removed successfully");
        }catch (Exception e){
            LOG.info("Exception in deleteHotel method for hotel ID: " + id);
        }
    }

    @Override
    public HotelEntity getHotelById(Long id) {
        return hotelRepository.findHotelById(id);
    }


}
