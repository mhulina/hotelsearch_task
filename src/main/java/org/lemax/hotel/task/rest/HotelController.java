package org.lemax.hotel.task.rest;

import org.lemax.hotel.task.model.Hotel;
import org.lemax.hotel.task.services.HotelServices;
import org.lemax.hotel.task.services.UserServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class HotelController {

    private final HotelServices hotelServices;
    private final UserServices userServices;

    private static final Logger LOG = LoggerFactory.getLogger(HotelController.class);

    public HotelController(final HotelServices hotelServices,
                           final UserServices userServices){
        this.hotelServices = hotelServices;
        this.userServices = userServices;
    }

    @GetMapping(value = "getHotels", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Hotel> showAll(){
        return hotelServices.getHotels();
    }

    @GetMapping(value = "getHotel/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Hotel getHotel(@PathVariable("id") Long id){
        return hotelServices.getHotelById(id);
    }

    @GetMapping(value = "searchHotels", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Hotel> searchHotels(){
        return hotelServices.searchHotels();
    }

    @PostMapping(value = "createHotel", produces = MediaType.APPLICATION_JSON_VALUE)
    public Hotel create(@RequestBody Hotel hotel){
        return hotelServices.createHotel(hotel);
    }

    @PostMapping(value = "createHotels", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Hotel> createBulk(@RequestBody List<Hotel> hotels){
        return hotelServices.createHotels(hotels);
    }

    @PutMapping(value = "updateHotel/{id}")
    public Hotel updateHotelData(@RequestBody Hotel newHotelData, final @PathVariable("id") Long id){
        return hotelServices.updateHotelData(newHotelData, id);
    }

    @DeleteMapping(value = "remove/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteHotelById(final @PathVariable("id") Long id){
        hotelServices.deleteHotelById(id);
    }
}
