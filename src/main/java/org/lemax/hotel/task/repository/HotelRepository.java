package org.lemax.hotel.task.repository;

import org.lemax.hotel.task.entity.HotelEntity;
import org.lemax.hotel.task.model.GeoLocation;
import org.lemax.hotel.task.model.Hotel;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository//RestResource(collectionResourceRel = "hotels", path = "hotels")
public interface HotelRepository extends PagingAndSortingRepository<HotelEntity, Long> {

    List<HotelEntity> findAll();
    HotelEntity findHotelById(@Param("id") Long id);
    List<HotelEntity> findByHotelName(@Param("name") String hotelName);
    List<HotelEntity> findByLatitudeAndLongitude(@Param("latitude") String latitude, @Param("longitude") String longitude);
    List<HotelEntity> findByHotelPrice(@Param("price") String hotelPrice);
    void deleteById(@Param("id") Long id);
}
